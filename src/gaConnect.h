/* Copyright (C) 2003-2008 by the 
 * Institute of Global Environment and Society (IGES)
 * See the file COPYRIGHT for more information.   
 *
 *
 * gaConnect.h  - an implementation of the OPeNDAP Connect class
 *
 * Last modified: $Date: 2008/07/25 20:43:25 $ 
 * Revision for this file: $Revision: 1.5 $
 * Release name: $Name: gadods-2_0 $
 * Original for this file: $Source: /homes/cvsroot/gadods/gadods/src/gaConnect.h,v $
 */

#ifndef _gaConnect_h_
#define _gaConnect_h_

#include <vector>
#include <utility>

#include "gaReports.h"

#include <Connect.h>
#include <AttrTable.h>
#include <DDS.h>
#include <DAS.h>

typedef struct gadap_attrib_struct {
  string name;
  string value;
} gadap_attrib;

class gaConnect: public Connect {

 DAS d_das;
 DDS d_dds;

 public:

  gaConnect(string name, string uname = "", string password = ""); 

/*****************************************************************
 * Identifying variables 
 */

  /* Returns the index of the variable with the specified name, 
   * Returns a negative error code if the variable is not present. */
  int getVarIndex(const char *varname);

/* Returns the index of the given variable, or
 * GADAP_ERR_NAME_NOT_FOUND.  Uses the fully qualified variable name,
 * which includes the inner and outer sequence names. */
  int getLongVarIndex(const char *longvarname);

  /* Returns the name of the nth variable */
  const char * getVarName(int index);

  /* Returns the type of the nth variable */
  GADAP_VAR_TYPE getVarType(int index);

  /* Returns the length of the given variable, or an error if not found */
  int getVarLen(int index);

  /* Returns the total number of variables */
  int getNumVars();

/* Returns the total number of level-independent variables.  
 * Level-independent variables come first in the variable list.
 * Thus, variables with indexes 0 through (numlivars - 1) are 
 * level-independent, while those with indexes >= numlivars have multiple
 * vertical levels. */
  int getNumLIVars();
  
  /*****************************************************************
   * Obtaining metadata 
   */
  
  /* Returns the short name of the dataset */
  const char * getName();

  /* Returns the full name of the dataset */
  const char * getFullName();

  /* Returns the URL used to open this dataset */
  const char * getURL();
  
  /* Returns the number of metadata attributes associated with this dataset */
  int getNumAttrs(int varindex);
  
  /* Returns the name of the nth attribute of the dataset */
  const char * getAttrName(int varindex, int index);
  
  /* Returns the index of the attribute with the specified name in the
     dataset. Returns a negative error code if the attribute is
     not present. */
  int getAttrIndex(int varindex, const char *attrname);

  /* Returns the value of the nth attribute of the dataset,
   * as a string. */
  const char * getAttr(int varindex, int index);
  
  /* Places the numeric value of the nth attribute of the 
   * dataset in the value argument. If the attribute cannnot be converted
   * to a numerical representation, value is not set, and an error is 
   * indicated in the return value */
  GADAP_STATUS getAttrDbl(int varindex, int index, double *value);

/*****************************************************************
 * Retrieving data 
 */


/* Generates constraint and url strings for the query structure,
 * based on the current query settings and the URL for this
 * dataset. */
  const char *getURL(GADAP_STN_QUERY *query);

/* Makes the specified query on the specified dataset. If successful, a 
 * handle to the resulting report collection is returned. The caller is 
 * responsible for disposing of the query once it has been used using
 * the appropriate library routine. */
  gaReports *getReports(GADAP_STN_QUERY *query);

/*****************************************************************
 * Internal members and structures
 */

 private: 

  // Helper functions
  void buildAttrList();
  void parseAttrTable(AttrTable *table, string base_name, int varindex);
  int convertGlobalIndex(int varIndex);
    
  // Data members
  vector<gadap_varinfo > variables;
  int numIVars;

  // Array of arrays of string pairs:
  // Outer index is variable id (globals are stored in entry [variables.size])
  // Inner index is attribute id
  // Pair contains attribute name, value
  //  vector<vector<pair<string,string> > > attributes;
  vector<vector<gadap_attrib> > attributes;

  string url;

};

#endif
