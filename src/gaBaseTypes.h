/* Copyright (C) 2003-2008 by the 
 * Institute of Global Environment and Society (IGES)
 * See the file COPYRIGHT for more information.   
 *
 *
 * gaBaseTypes.h  - Declarations for the OPeNDAP basetypes that have added functionality 
 *                  and thus might be typecast to in other source files.
 * 
 * Last modified: $Date: 2008/07/25 20:43:25 $ 
 * Revision for this file: $Revision: 1.2 $
 * Release name: $Name: gadods-2_0 $
 * Original for this file: $Source: /homes/cvsroot/gadods/gadods/src/gaBaseTypes.h,v $
 */

#include "Str.h"
#include "Url.h"

/* These basetypes have a new public function, getStringVal().  This
 * is because I don't want the gadods library to require the client to
 * allocate and free memory when reading data  out of a report
 * collection.  
 * 
 * The standard way to get a value out of a BaseType object is using
 * buf2val(). However, this method does not provide access directly to
 * the value that is already in memory - it copies that value into a
 * separate buffer that must then be freed at some point by the
 * client.  getStringVal() wraps buf2val() and instead, provides the
 * client with access to an internal copy of the value. This way, the
 * string value gets deleted automatically along with the BaseType
 * object, which I think is a lot more friendly and less leak-prone.
 * 
 */


class gaStr: public Str {
public:
  gaStr(const string &n = "");
  virtual ~gaStr();
  virtual BaseType *ptr_duplicate();
  virtual bool read(const string &dataset);

  /* Provide access to an internal (i.e. memory-managed by the gadods
     library) copy of the value of this variable */
  virtual string * getStringVal();
 protected:
  string * publicval;
};

class gaUrl: public Url {
public:
  gaUrl(const string &n = "");
  virtual ~gaUrl();
  virtual BaseType *ptr_duplicate();
  virtual bool read(const string &dataset);

  /* Provide access to an internal (i.e. memory-managed by the gadods
     library) copy of the value of this variable */
  virtual string * getStringVal();
 protected:
  string * publicval;
};
