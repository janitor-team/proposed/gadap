
// -*- mode: c++; c-basic-offset:4 -*-

// This file is part of libdap, A C++ implementation of the OPeNDAP Data
// Access Protocol.

// Copyright (c) 2005 OPeNDAP, Inc.
// Author: James Gallagher <jgallagher@opendap.org>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// You can contact OPeNDAP, Inc. at PO Box 112, Saunderstown, RI. 02874-0112.


#include <string>

#if 0
#include "gaByte.h"
#include "gaInt16.h"
#include "gaUInt16.h"
#include "gaInt32.h"
#include "gaUInt32.h"
#include "gaFloat32.h"
#include "gaFloat64.h"
#include "gaStr.h"
#include "gaUrl.h"
#include "gaArray.h"
#include "gaStructure.h"
#include "gaSequence.h"
#include "gaGrid.h"
#endif

#include "gaTypeFactory.h"
#include "debug.h"

Byte *
gaTypeFactory::NewByte(const string &n ) const 
{ 
    return new gaByte(n);
}

Int16 *
gaTypeFactory::NewInt16(const string &n ) const 
{ 
    return new gaInt16(n); 
}

UInt16 *
gaTypeFactory::NewUInt16(const string &n ) const 
{ 
    return new gaUInt16(n);
}

Int32 *
gaTypeFactory::NewInt32(const string &n ) const 
{ 
    DBG(cerr << "Inside gaTypeFactory::NewInt32" << endl);
    return new gaInt32(n);
}

UInt32 *
gaTypeFactory::NewUInt32(const string &n ) const 
{ 
    return new gaUInt32(n);
}

Float32 *
gaTypeFactory::NewFloat32(const string &n ) const 
{ 
    return new gaFloat32(n);
}

Float64 *
gaTypeFactory::NewFloat64(const string &n ) const 
{ 
    return new gaFloat64(n);
}

Str *
gaTypeFactory::NewStr(const string &n ) const 
{ 
    return new gaStr(n);
}

Url *
gaTypeFactory::NewUrl(const string &n ) const 
{ 
    return new gaUrl(n);
}

Array *
gaTypeFactory::NewArray(const string &n , BaseType *v) const 
{ 
    return new gaArray(n, v);
}

Structure *
gaTypeFactory::NewStructure(const string &n ) const 
{ 
    return new gaStructure(n);
}

Sequence *
gaTypeFactory::NewSequence(const string &n ) const 
{ 
    DBG(cerr << "Inside gaTypeFactory::NewSequence" << endl);
    return new gaSequence(n);
}

Grid *
gaTypeFactory::NewGrid(const string &n ) const 
{ 
    return new gaGrid(n);
}
