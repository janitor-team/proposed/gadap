/* Copyright (C) 2003-2008 by the 
 * Institute of Global Environment and Society (IGES)
 * See the file COPYRIGHT for more information.   
 *
 *
 * gaUtils.cc - Some functions and structures shared by gaConnect and
 *  gaReports, for parsing the DDS. Really this should be rolled into
 *  a class, gaVars, that both gaConnect and gaReports have an
 *  instance of.
 *
 * Last modified: $Date: 2008/07/25 20:43:25 $ 
 * Revision for this file: $Revision: 1.3 $
 * Release name: $Name: gadods-2_0 $
 * Original for this file: $Source: /homes/cvsroot/gadods/gadods/src/gaUtils.h,v $
 */

#ifndef _gaUtils_h_
#define _gaUtils_h_

#include <vector>
#include "DDS.h"
#include <string>
#include "Sequence.h"
#include "gadap.h"

typedef struct varinfo {
  string name;
  string longname;
  GADAP_VAR_TYPE type;
  int length;
} gadap_varinfo;

/** Generates a vector of gadap_varinfo structures from a DAP DDS object. */
void parseVars(vector<gadap_varinfo> &variables, DDS &dds, int &numIVars);

/** Finds the first occurence of a Sequence variable at the outermost
    level of a DAP DDS. */
Sequence * findOuterSequence(DDS &dds);

/** Finds the first occurence of a Sequence variable nested inside
    another Sequence variable. */
Sequence * findInnerSequence(Sequence * outer);

/** Strips inital and final quotation marks (") from the string given */
string &removeQuotes(string &input);

#endif
