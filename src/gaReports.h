/* Copyright (C) 2003-2008 by the 
 * Institute of Global Environment and Society (IGES)
 * See the file COPYRIGHT for more information.   
 *
 *
 * gaReports.h  - The library's container for station data reports
 *
 * Last modified: $Date: 2008/07/25 20:43:25 $ 
 * Revision for this file: $Revision: 1.4 $
 * Release name: $Name: gadods-2_0 $
 * Original for this file: $Source: /homes/cvsroot/gadods/gadods/src/gaReports.h,v $
 */

#ifndef _gaReports_h_
#define _gaReports_h_

#include "gadap.h"
#include <DDS.h>
#include <Sequence.h>
#include "gaUtils.h"

class gaReports {

 public: 
  
  gaReports(DDS *_data, string url);

  ~gaReports();

  /* Returns handle to the dataset from which these reports were obtained */
  GADAP_DATASET getDataset();
  void setDataset(GADAP_DATASET _dataset);

  /* Returns the query that was used to generate this report */
  GADAP_STN_QUERY *getQuery();
  void setQuery(GADAP_STN_QUERY *_query);
  
  /* Returns the URL that was sent to the server to generate this report */
  const char * getURL();

  /* Total number of variables used by this report collection */
  int getNumVars();
  
  /* Total number of level-dependent variables in the dataset */
  int getNumLIVars();
  
  /* Name of the nth variable in this report collection  */
  const char *getVarName(int index);

  /* Name of the nth variable in this report collection  */
  int getVarLen(int index);
  
  /* Name of the nth variable in this report collection  */
  int getVarType(int index);

  /* Returns the index of the given variable, or -1 if not found */
  int getVarIndex(const char *varname);

  int getNumReports();

  int getNumLevels(int rpt_index);
  
  /* Returns the numeric value of the given variable in the given report */
  GADAP_STATUS getVarDbl(int rpt_index, int lev_index, int var_index, int array_index,
			  double *value);
  
  /* Returns the string value of the given variable in the given report */
  const char * getVarStr(int rpt_index, int lev_index, int var_index, int array_index);
  
 private:

  BaseType * unpackArray(BaseType * var, int array_index);
  
  BaseType * getVarPtr(int rpt_index, int lev_index, int var_index);
    
  Sequence *getLevels(int rpt_index);
  
  GADAP_DATASET dataset;
  string url;
  vector<gadap_varinfo> variables;
  DDS *data;
  Sequence *reports, *levels;
  bool hasLevels;
  GADAP_STN_QUERY *query;
  int stidIndex, timeIndex, latIndex, lonIndex, levIndex;
  int numIVars;

};

  
#endif
